const express = require('express');
const logger = require('morgan');
const app = express();

app.use(logger());
app.use(express.static('./public'));
app.listen(8080, () => console.log('Ready'));
