# AWS infrastructure for experimenting with new services

## Getting started

#### AWS credentials

First you need to set up credentials in your shell so you can use aws-cli. I recommend https://github.com/99designs/aws-vault .

#### Cuffsert

The examples in this readme makes use of https://github.com/burtcorp/cuffsert to load CloudFormation stacks, since it is a hassle to load via awscli or CloudFormation UI. The easiest way to install cuffsert is as a global gem:

```bash
sudo apt-get install ruby2.5
sudo gem install cuffsert
```

## VPC

Other stacks depend on having a VPC to install services into (cuffsert currently has a bug, so we need to pass values for parameters that have defaults in the stack, namely the various CIDRs).

```bash
cuffsert --region eu-north-1 -v --ask -n $(id -un)-vpc \
    -p VpcName=$(id -un)-vpc \
    -p VpcCIDR=10.192.0.0/16 \
    -p PublicSubnet1CIDR=10.192.10.0/24 \
    -p PublicSubnet2CIDR=10.192.11.0/24 \
    vpc.yml
```

## Docker Swarm

Set up a simple swarm cluster so we can experiment with services. The stack has the following parameters:

* **VpcName**: must match VpcName from the loaded VPC above.
* **SwarmName**: choose a name for your swarm. Must be unique in the region.
* **ManagerInstanceType**: An EC2 instance type. Currently the setup is tested with the t3 instance family, so `t3.small`, `t3.medium` or `t3.xlarge` would be good candidates. Please mind the cost.
* **KeyName**: Name of a SSH "keypair" uploaded to EC2. You may want to `aws ec2 --region eu-north-1 import-key-pair --key-name $(id -un) --public-key-material "$(cat ~/.ssh/id_rsa.pub)"`
* **MyIP**: Set to a CIDR that identifies the IP or net you want the swarm to be accessible from.
* **TargetGroupArn**: Arn of the load balancer for your swarm (read below), or "" if you do not want a load balancer. AWS will automatically add all your manager nodes to this target group. You can find the Arn with ``aws --region eu-north-1 cloudformation describe-stack-resources --stack-name $(id -un)-swarm-loadbalancer --query 'StackResources[?ResourceType==`AWS::ElasticLoadBalancingV2::TargetGroup`].PhysicalResourceId' --output text``

```bash
cuffsert --region eu-north-1 -v --ask -n $(id -un)-swarm \
    -p VpcName=$(id -un)-vpc \
    -p SwarmName=$(id -un)-swarm \
    -p ManagerInstanceType=t3.medium \
    -p KeyName=$(id -un) \
    -p MyIP=$(curl -s4 ifconfig.co)/32 \
    swarm.yml
```
Once the stack has been loaded, you probably want to monitor progress setting up the instances in the AWS EC2 console. You can find the public IP of a manager node:

```bash
MANAGER_IP=$(aws --region eu-north-1 ec2 describe-instances --instance-ids $(aws --region eu-north-1 autoscaling describe-auto-scaling-groups --query 'AutoScalingGroups[?starts_with(AutoScalingGroupName, `'$(id -un)'-swarm-ManagerASG`)].Instances[0].InstanceId' --output text) --query 'Reservations[].Instances[].PublicIpAddress' --output text)
```

#### Re-scaling the cluster

The cluster starts with a single manager node. When you want to add more nodes, you run this command:

```bash
aws --region eu-north-1 autoscaling set-desired-capacity \
    --auto-scaling-group-name $(aws --region eu-north-1 autoscaling describe-auto-scaling-groups --query 'AutoScalingGroups[?starts_with(AutoScalingGroupName, `'$(id -un)'-swarm-ManagerASG`)].AutoScalingGroupName' --output text) \
    --desired-capacity 3
```

The Swarm stack currently cannot handle being scaled down to 0 nodes. The issue can also occur if node setup fails. In order to recover from this state, remove the swarm join token:

```bash
aws --region eu-north-1 ssm delete-parameter --name /$(id -un)-swarm/join-token
```

Now the first manager node will generate and write a new join token that further nodes can read.

## Load balancer

In order to allow general access to your swarm, you need a load balancer. Load this stack to make services in your swarm publicly available. If you configure the swarm stack with a TargetGroupArn, AWS will automatically add all your manager nodes to the load balancer. You may get into trouble if your service is served by only some of the nodes since there is no health check to eject the the non-responsive nodes from the load balancer. You need the following parameters when loading your stack:

* **VpcName**: must match VpcName from the loaded VPC above.
* **SwarmName**: must match SwarmName above.
* **ServiceName**: hostname part; your service will be available on https://<servicename>.pilotfish.academy/
* **ServiceTargetPort**: Port on the manager nodes where your HTTP service is exposed.
* **ValidationSource**: The hostname part of the validation info, something like \_hash.<servicename>.pilotfish.academy.
* **ValidationTarget**: The CNAME part of the validation info, something like \_hash.asdfasdf.acm-validations.aws.

This stack adds a certificate so that your load balancer can serve HTTPS. However, in order to validate that you have control over the domain name in question, you have to add a DNS record to Route 53. That record can not be known in advance :/. Therefore, you must create the stack once, read the output, abort creation and create the stack again, this time supplying `ValidationSource` and `ValidationTarget`. Thus:

```bash
cuffsert --region eu-north-1 -v --ask -n (id -un)-swarm-loadbalancer \
    -p VpcName=(id -un)-vpc \
    -p SwarmName=(id -un)-swarm \
    -p ServiceName=some-service \
    -p ServiceTargetPort=8080 \
    -p ValidationSource="" \
    -p ValidationTarget="" \
    load-balancer.yml
```

Wait until the certificate validation info shows up and then delete the stacK:
```bash
aws --region eu-north-1 cloudformation delete-stack --stack-name (id -un)-swarm-loadbalancer
```

Then create the stack again with the validation info. It can take several minutes for the certificate to be validated, so please be patient:

```bash
cuffsert --region eu-north-1 -v --ask -n (id -un)-swarm-loadbalancer \
    -p VpcName=(id -un)-vpc \
    -p SwarmName=(id -un)-swarm \
    -p ServiceName=some-service \
    -p ServiceTargetPort=8080 \
    -p ValidationSource=_a98759875c987ad7a98f5a8975c9f987.some-service.pilotfish.academy. \
    -p ValidationTarget=_19s8cb5980590587csab508as7b5s08c.xxxxxxxxxx.acm-validations.aws. \
    load-balancer.yml
```

Now you should be able to update the swarm to point the it to the loead balancer's target group:

```bash
cuffsert --region eu-north-1 -v --ask -n $(id -un)-swarm \
    -p TargetGroupArn=$(aws --region eu-north-1 elbv2 describe-target-groups --query 'TargetGroups[?TargetGroupName == `some-service`].TargetGroupArn' --output text) \
    swarm.yml
```

## IoT-to-Google Maps

This is a simple serverless project which allows reporting GGA NMEA 0183 lines to an AWS IoT topic and watch the corresponding vehicle move about on Google Maps. For this to work, you need a GCP account and an API key which allows access to the Maps Javascript API.

First, you need to create IoT certificates for the vehicle(s). The CloudFormation stack contains two hardcoded vehicles, so you will need to do this twice:

```bash
THING=vehicle-1
aws --region eu-north-1 iot create-keys-and-certificate --set-as-active > $THING.json
echo -e $(jq .certificatePem < $THING.json | tr -d '"') > $THING.cert
echo -e $(jq .keyPair.PrivateKey < $THING.json | tr -d '"') > $THING.key
jq -r .certificateArn < $THING.json
```

The stack also sets up a domainname and CloudFront dsitro to serve the Maps frontend, so you will need a Route 53 hostedzone id and an ACM ARN for that domainname. Armed with this info, you can now load the stack:

```bash
cuffsert --region eu-north-1 --name iot-positioning \
    -p Vehicle2CertificateArn=<vehicle-1-arn> \
    -p Vehicle2CertificateArn=<vehicle-2-arn> \
    -p MapHostedZoneId=<hostedzone id> \
    -p MapDomainName=<frontend domainname> \
    -p MapCertificateArn=<ACM ARN for frontend domainname> \
    ./iot-positioning.yml
```

Get the hostname for AWS IoT endpoint:

```bash
ENDPOINT=$(aws iot describe-endpoint --region eu-north-1 --query endpointAddress --output text)
wget https://www.amazontrust.com/repository/AmazonRootCA1.pem -O amazon-root-ca.pem
```

If everything works, you should be able to insert messages and se the position update lambda react:

```bash
mosquitto_pub -h $ENDPOINT --cert vehicle-1.cert --key vehicle-1.key --cafile ./amazon-root-ca.pem  -t position/vehicle-1 -m '$GNGGA,222718.00,5743.80245,N,01159.75860,E,2,12,0.69,1.9,M,38.2,M,,0000*4E' -d
```

You can verify that your position has been storead and can be retrieved:

```bash
curl https://$(aws --region eu-north-1 apigateway get-rest-apis --query 'items[?name==`Positioning API`].id' --output text).execute-api.eu-north-1.amazonaws.com/v1/positions/vehicle-1/latest
```

Now that we know this works, we can test the frontend locally using the included dev server. You probably need to edit iot-positioning/public/index.html in order to set your API key and the corrent API endpoint as per above.

```bash
cd iot-positioning
npm install
node ./index.js
xdg-open http://lvh.me:8080/
```

Once you are satisfied it works, you can deploy the frontend to S3 from whence CloudFront will serve it:
```bash
aws s3 cp iot-positioning/public/index.html s3://iot-positioning-mapbucket-<hash>/index.html
```

## Services

#### elasticsearch.yml

This stack combines an ElasticSearch clustrer and a Traefik (a Docker Swarm-aware load balancer) which exposes ES on port 9200 on all management nodes. ElasticSearch depends on dnsrr mode to discover cluster members.

```bash
sed -re "s/SWARM_ZONE/$(id -un)-swarm.$(id -un)-vpc/g" elasticsearch.yml | ssh ec2-user@${MANAGER_IP} docker stack deploy -c - elasticsearch
```

Loading some data into ElasticSearch (run from one of the manager nodes):

```bash
cat <<EOF | ssh ec2-user@${MANAGER_IP} bash
sudo yum install -y python3-pip
sudo pip3 install elasticsearch-loader
wget https://data.gharchive.org/2019-11-01-15.json.gz
zcat 2019-11-01-15.json.gz | elasticsearch_loader --es-host http://manager.$(id -un)-swarm.$(id -un)-vpc:9200 --index gharchive --progress --id-field id --with-retry json --lines /dev/stdin
EOF
```

There are some entries in the data that seems not to load, resulting in confusing warnings, but most of the records seems to load.
